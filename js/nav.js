$(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 10) {
        $("nav").addClass("scrolled");
    } else {
        $("nav").removeClass("scrolled");
    }
});

$(".product").mouseenter(function(){
    $(".productDropdown").show();
});

$(".productDropdown").mouseleave(function(){
    $(".productDropdown").hide();
});

$('.menuButton').click(function(){
    $('.mobileMenu').toggle();
    $('nav').toggleClass('selected');
})


